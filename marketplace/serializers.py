from marketplace.models import MarketPlace, Product
from rest_framework import serializers


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'name', 'price', 'marketplace']
        read_only_fields = []


class MarketPlaceSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = MarketPlace
        fields = [
            'id', 'name', 'federal_registration', 'products',
            'user', 'image', 'latitude', 'longitude', 'active']
        read_only_fields = []


class MarketPlaceNestSerializer(serializers.ModelSerializer):

    class Meta:
        model = MarketPlace
        fields = [
            'id', 'name', 'federal_registration']
        read_only_fields = []
