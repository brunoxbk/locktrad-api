from marketplace.models import MarketPlace, Product
from marketplace.serializers import MarketPlaceSerializer, ProductSerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.shortcuts import get_object_or_404
import datetime
import random
import json


class MarketPlaceList(generics.ListCreateAPIView):
    queryset = MarketPlace.objects.filter()
    serializer_class = MarketPlaceSerializer


class MarketPlaceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MarketPlace.objects.filter()
    serializer_class = MarketPlaceSerializer


class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.filter()
    serializer_class = ProductSerializer


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.filter()
    serializer_class = ProductSerializer
