from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.conf import settings
import uuid
import os
from django.urls import reverse_lazy, reverse
from django.db.models import Count
from django.db.models.functions import TruncMonth
# from django.contrib.gis.db import models as gis_models


class MarketPlace(models.Model):
    name = models.CharField(_("nome"), max_length=120)
    federal_registration = models.CharField(_("CNPJ"), max_length=120)
    user = models.ForeignKey(
        "users.User", verbose_name="Proprietário",
        related_name='marketplaces', on_delete=models.CASCADE)

    image = models.ImageField(
        upload_to='marketplace/',
        verbose_name="Imagem", blank=True, null=True)

    latitude = models.FloatField('latitude', blank=True, null=True)
    longitude = models.FloatField('longitude', blank=True, null=True)

    created_at = models.DateTimeField(_('criado em'), auto_now_add=True)
    updated_at = models.DateTimeField(_('alterado em'), auto_now=True)
    deleted_at = models.DateTimeField(_('deletado em'), null=True, blank=True)
    active = models.BooleanField(_('ativo'), default=True)

    class Meta:
        verbose_name = _("mercado")
        verbose_name_plural = _("mercados")
        get_latest_by = "created_at"
        ordering = ['-created_at']

    def __str__(self):
        return self.name


class Product(models.Model):
    marketplace = models.ForeignKey(
        "marketplace.MarketPlace", verbose_name="Mercado",
        related_name='products', on_delete=models.CASCADE)
    name = models.CharField(_("nome"), max_length=120)
    price = models.DecimalField(
        'preço', blank=True, null=True, decimal_places=2, max_digits=9)

    created_at = models.DateTimeField(_('criado em'), auto_now_add=True)
    updated_at = models.DateTimeField(_('alterado em'), auto_now=True)
    deleted_at = models.DateTimeField(_('deletado em'), null=True, blank=True)
    active = models.BooleanField(_('ativo'), default=True)

    class Meta:
        verbose_name = _("preço")
        verbose_name_plural = _("preços")
        get_latest_by = "created_at"
        ordering = ['-created_at']

    def __str__(self):
        return self.name
