from django.urls import path
from marketplace import api


app_name = 'marketplace'


urlpatterns = [
    path('', api.MarketPlaceList.as_view(), name='list'),
    path('<int:pk>/',
         api.MarketPlaceDetail.as_view(), name='detail'),
    path('product/', api.ProductList.as_view(), name='product-list'),
    path('product/<int:pk>/',
         api.ProductDetail.as_view(), name='product-detail'),
]
