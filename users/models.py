import re
from django.db import models
from django.core import validators
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.conf import settings
from datetime import datetime, timedelta


class UserManager(BaseUserManager):
    def _create_user(self, username, email, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError(_("The given username must be set"))
        email = self.normalize_email(email)
        user = self.model(
            username=username, email=email,
            is_staff=is_staff, is_active=True,
            is_superuser=is_superuser, last_login=now,
            date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        user = self._create_user(
            username, email, password, True, True, **extra_fields)
        user.is_active = True
        user.save(using=self._db)
        return user


username_validators = [
    validators.RegexValidator(
        re.compile('^[\w.@+-]+$'),
        _("Enter a valid username."),
        _('invalid'))
]


class User(AbstractBaseUser, PermissionsMixin):

    first_name = models.CharField(
        _("first name"), max_length=30, blank=False, null=False)
    last_name = models.CharField(
        _("last name"), max_length=30, blank=False, null=False)
    email = models.EmailField(
        _("email address"), max_length=255, unique=True, blank=False, null=False)
    is_staff = models.BooleanField(
        _("staff status"), default=False,
        help_text=_("Designates whether the user can log into this admin site."))
    is_active = models.BooleanField(
        _("active"), default=True,
        help_text=_("Designates whether this user should be treated as active. \
        Unselect this instead of deleting accounts."))
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    is_trusty = models.BooleanField(
        _("trusty"), default=False,
        help_text=_("Designates whether this user has confirmed his account."))

    is_seller = models.BooleanField(_('vendedor'), default=False)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]

    objects = UserManager()

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
