from django.urls import path
from users import api


app_name = 'users'


urlpatterns = [
    path('api/', api.UserList.as_view(), name='list'),
    path('api/<int:pk>/',
         api.UserDetail.as_view(), name='detail'),
    path('register/', api.RegisterView.as_view(), name='register'),
    path('comercial/register/', api.RegisterComercialView.as_view(),
         name='register-comercial'),
    path('get-token/', api.CustomAuthToken.as_view(), name='token'),
]
