from users.models import User
from users.serializers import UserSerializer, RegisterComercialSerializer
from marketplace.serializers import MarketPlaceNestSerializer
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import generics
from .serializers import RegisterSerializer


class UserList(generics.ListCreateAPIView):
    queryset = User.objects.filter()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.filter()
    serializer_class = UserSerializer


class CustomAuthToken(ObtainAuthToken):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        return Response({
            'token': token.key,
            'user': {
                'id': user.pk,
                'email': user.email,
                'first_name': user.first_name,
                'is_seller': user.is_seller,
                'marketplaces': MarketPlaceNestSerializer(user.marketplaces.all(), many=True).data
            }})


class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class RegisterComercialView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterComercialSerializer
