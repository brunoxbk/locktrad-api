from users.models import User
from marketplace.models import MarketPlace
from marketplace.serializers import MarketPlaceNestSerializer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from django.db import transaction


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('password', 'password2',
                  'email', 'first_name', 'last_name', 'is_seller')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            is_seller=validated_data['is_seller']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class RegisterComercialSerializer(serializers.ModelSerializer):
    #-5.064812, -42.781463
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    # comercial
    federal_registration = serializers.CharField(
        write_only=True, required=True)
    name = serializers.CharField(write_only=True, required=True)
    image = serializers.ImageField(required=False)
    latitude = serializers.FloatField(write_only=True, required=True)
    longitude = serializers.FloatField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'password', 'password2', 'latitude', 'longitude',
            'image', 'email', 'first_name', 'last_name',
            'federal_registration', 'is_seller', 'name')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})

        return attrs

    @transaction.atomic
    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            is_seller=validated_data['is_seller']
        )

        user.set_password(validated_data['password'])
        user.save()

        marketplace = MarketPlace(
            name=validated_data['name'],
            federal_registration=validated_data['federal_registration'],
            user=user,
            latitude=validated_data['latitude'],
            longitude=validated_data['longitude']
        )

        if 'image' in validated_data:
            marketplace.image = validated_data['image']

        marketplace.save()

        return user


class UserSerializer(serializers.ModelSerializer):
    marketplaces = MarketPlaceNestSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name',
                  'email', 'is_seller', 'marketplaces']
        read_only_fields = []
